package view;

/* **************************
   *        Legend          *
   *------------------------*
   *     p_ = panel         *
   *     l_ = label         *
   *     b_ = button        *
   *   tf_ = text field     *
   **************************/

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.Border;


/**
 * The Login Interface
 */
public class Login extends JFrame{

    //The text fields needed
    private JTextField tf_userID = new JTextField("", 15);
    private JTextField tf_password = new JTextField("", 15);

    //The buttons needed
    private JButton b_signUp = new JButton("Sign Up");
    private JButton b_login = new JButton("Log In");

    /**
     * Constructor creates the Login interface
     */
    public Login(){

        this.setTitle("Health Tracker"); ///< Set the title of the frame
        this.setSize(300, 200); ///< Set the size of the frame
        this.setResizable(false); ///< Make the frame not resizeable
        this.setLocationRelativeTo(null); ///< Set the position of the frame at the center of the screen

        //Set the buttons tooltips
        b_signUp.setToolTipText("Click here to go to the registration page");
        b_login.setToolTipText("Click here to log in");

        JPanel p_login = new JPanel(); ///< The panels needed for this view


        Border loginBorder = BorderFactory.createTitledBorder("Log In"); ///< Create the border
        p_login.setBorder(loginBorder); ///< Set the border
        p_login.setLayout(new GridBagLayout()); ///< Set the layout

        JLabel l_userID = new JLabel("User ID: "); ///< User ID label
        addComponent(p_login, l_userID, 0, 0, 1, 1,
                     GridBagConstraints.WEST, GridBagConstraints.NONE);

        tf_userID.setToolTipText("Please enter the user ID"); ///< User ID text field
        addComponent(p_login, tf_userID, 1, 0, 2, 1,
                     GridBagConstraints.EAST, GridBagConstraints.NONE);

        JLabel l_password = new JLabel("Password: "); ///< Password label
        addComponent(p_login, l_password, 0, 1, 1, 1,
                     GridBagConstraints.WEST, GridBagConstraints.NONE);

        tf_password.setToolTipText("Please enter the password"); ///< Password text field tooltip
        addComponent(p_login, tf_password, 1, 1, 2, 1,
                     GridBagConstraints.EAST, GridBagConstraints.NONE);

        //The sign up and login buttons
        addComponent(p_login, b_signUp, 0, 2, 1, 1,
                     GridBagConstraints.WEST, GridBagConstraints.NONE);

        addComponent(p_login, b_login, 1, 2, 1, 1,
                GridBagConstraints.EAST, GridBagConstraints.NONE);

        this.add(p_login); ///< Add the main panel to the frame
        this.setVisible(true); ///< Show the frame
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); ///< Exit when close button is clicked
    }



    /**
     * Method to add a java component using the gridbag layout
     *
     * @param currentPanel The panel to add the component to
     * @param comp The component to be added
     * @param xPos The x position of the component
     * @param yPos The y position of the component
     * @param compWidth The width of the component
     * @param compHeight The height of the component
     * @param place The anchor of the component
     * @param stretch How the component is to be stretched
     */
    public static void addComponent(JPanel currentPanel, JComponent comp, int xPos, int yPos,
                                    int compWidth, int compHeight, int place, int stretch){

        GridBagConstraints gridConstraints = new GridBagConstraints();

        gridConstraints.gridx = xPos;
        gridConstraints.gridy = yPos;
        gridConstraints.gridwidth = compWidth;
        gridConstraints.gridheight = compHeight;
        gridConstraints.weightx = 100;
        gridConstraints.weighty = 100;
        gridConstraints.insets = new Insets(5, 5, 5, 5);
        gridConstraints.anchor = place;
        gridConstraints.fill = stretch;

        currentPanel.add(comp, gridConstraints);
    }

    /**
     * To get the Username entered
     * @return the text in the user name text field
     */
    public String getUserID(){
        return tf_userID.getText();
    }

    /**
     *To get the Password entered
     * @return the text in the password text field
     */
    public String getPassword(){
        return tf_password.getText();
    }

    /**
     * Listen for the Log In button
     * @param listenerForLoginButton
     */
    public void addLoginListener(ActionListener listenerForLoginButton){
        b_login.addActionListener(listenerForLoginButton);
    }

    /**
     * Listen for the Sign Up button
     * @param listenerForSignUpButton
     */
    public void addSignUpListener(ActionListener listenerForSignUpButton){
        b_signUp.addActionListener(listenerForSignUpButton);
    }

    /**
     * Display an error message
     * @param errorMessage
     */
    public void displayErrorMessage(String errorMessage){
        JOptionPane.showMessageDialog(this, errorMessage);
    }

}
