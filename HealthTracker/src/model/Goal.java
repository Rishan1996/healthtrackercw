package model;

import java.time.*;

/**
 * Model of the Goal
 */
public class Goal {

    private String name = "";
    private String goalID = "";
    private String type = "";
    private LocalDateTime deadline = LocalDateTime.of(2017, Month.AUGUST, 20, 12, 00);
    private Boolean achieved = false;

    /**
     * Default Constructor
     */
    public Goal(){

    }

    /**
     * Constructor to set all the fields of Goal
     * @param name The title of the goal
     * @param goalID The userID this goal is associated with
     * @param type The type of goal it is
     * @param deadline The deadline for the goal
     * @param achieved If the goal has been achieved
     */
    public Goal(String name, String goalID, String type, LocalDateTime deadline, Boolean achieved){
        this.name = name;
        this.goalID = goalID;
        this.type = type;
        this.deadline = deadline;
        this.achieved = achieved;
    }



    //Accessor Methods
    public String getName(){return name;}
    public String getGoalID(){return goalID;}
    public String getType(){return type;}
    public LocalDateTime getDeadline(){return deadline;}
    public Boolean getAchieved(){return achieved;}

    //Mutator Methods
    public void setName(String name){this.name = name;}
    public void setGoalID(String goalID){this.goalID = goalID; }
    public void setType(String type){this.type = type;}
    public void setDeadline(LocalDateTime deadline){this.deadline = deadline;}
    public void setAchieved(Boolean achieved){this.achieved = achieved;}


}
