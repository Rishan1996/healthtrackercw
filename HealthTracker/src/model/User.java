package model;

/**
 * Model of the User
 */
public class User {

    private String name = "", userID = "", password = "", email = "";
    private int age = 0, weight = 0, height = 0;

    /**
     * Constructor
     */
    public User(){
    }

    //Accessor Methods
    public String getName(){
        return name;
    }
    public String getUserID(){
        return userID;
    }
    public String getPassword(){
        return password;
    }
    public String getEmail() {
        return email;
    }
    public int getAge(){
        return age;
    }
    public int getWeight(){
        return weight;
    }
    public int getHeight(){
        return height;
    }

    //Mutator Methods
    public void setName(String name){
        this.name = name;
    }
    public void setUserID(String userID){
        this.userID = userID;
    }
    public void setPassword(String password){
        this.password = password;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setAge(int age){
        this.age = age;
    }
    public void setWeight(int weight){
        this.weight = weight;
    }
    public void setHeight(int height){
        this.height = height;
    }

    /**
     * Method to check if user credentials match
     * @param userID The userID to be checked against the model
     * @param password The password to be checked against the model
     * @return true if the credentials are correct, else return false
     */
    public boolean checkCredential(String userID, String password){
        return this.userID.equals(userID) && this.password.equals(password);
    }




}
