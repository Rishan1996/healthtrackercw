package controller;

import jdk.nashorn.internal.scripts.JO;
import model.Goal;
import model.User;

import view.Registration;
import view.Login;
import view.Profile;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Coordinates interactions between the Viewsand the Models
 */
public class ModelController {

    //The views
    private Login loginView = null;
    private Registration registrationView = null;
    private Profile profileView = null;

    //The file storage controller
    FileStorageController fileStorage = new FileStorageController();

    //The models
    private User user = null;
    private Goal currentGoal = new Goal();

    /**
     * The constructor loads the views and sets up the listeners
     */
    public ModelController(){

        if(this.loginView == null)
            this.loginView = new Login();

        this.loginView.setVisible(true);

        this.loginView.addLoginListener(new LoginListener());
        this.loginView.addSignUpListener(new SignUpListener());

        if(this.registrationView == null)
            this.registrationView = new Registration();

        this.registrationView.setVisible(false);

        this.registrationView.addCancelListener(new CancelListener());
        this.registrationView.addRegisterListener(new RegisterListener());

    }

    private void displayProfile(User currentUser){

    }

    private void login(String username){
        //Close the login view and registration view
        loginView.dispose();
        loginView.setVisible(false);
        registrationView.dispose();
        registrationView.setVisible(false);
    }

    private boolean createGoal(User currentUser, String goalName, String userName){

        return false;
    }


    class LoginListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            String userIDField = loginView.getUserID();
            String passwordField = loginView.getPassword();

            //Check if both the username and password field is empty
            if(userIDField.isEmpty() && passwordField.isEmpty()){
                JOptionPane.showMessageDialog(loginView, "Username and password field is empty");
                return;
                //Check if the username field is empty or contains just a space
            }else if(userIDField.isEmpty() || userIDField.equals(" ")){
                JOptionPane.showMessageDialog(loginView, "Username field is empty or invalid");
                return;
                //Check if the password field is empty or contains a space
            }else if(passwordField.isEmpty() || passwordField.equals(" ")){
                JOptionPane.showMessageDialog(loginView, "Password field is empty or invalid");
                return;
            }

            //Attempt to login
            if (user.checkCredential(userIDField, passwordField)){


                //Load the profile page
                displayProfile(user);

            } else
                JOptionPane.showMessageDialog(loginView, "Incorrect credentials");


        }
    }

    class SignUpListener implements ActionListener{

        public void actionPerformed(ActionEvent event) {
            loginView.dispose();

            if(registrationView == null)
                registrationView = new Registration();

            loginView.setVisible(false);
            registrationView.setVisible(true);
        }
    }

    class CancelListener implements ActionListener{

        public void actionPerformed(ActionEvent event){
            registrationView.dispose();

            if(loginView == null)
                loginView = new Login();

            registrationView.setVisible(false);
            loginView.setVisible(true);
        }
    }

    class RegisterListener implements ActionListener{

        public final Pattern VALID_EMAIL_ADDRESS_REGEX =
                Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

        public void actionPerformed(ActionEvent event){

            String userIDField = registrationView.getUserID();
            String passwordField = registrationView.getPassword();
            String confirmPassField = registrationView.getConfirmPassword();
            String emailField = registrationView.getEmail();

            String firstNameField = registrationView.getFirstName();
            String surnameField = registrationView.getSurname();
            String ageField = registrationView.getAge();

            String heightField = registrationView.getUserHeight();
            String weightField = registrationView.getWeight();

            //Check if any fields are empty
            if(userIDField.isEmpty() && passwordField.isEmpty() && confirmPassField.isEmpty() && emailField.isEmpty()){
                JOptionPane.showMessageDialog(registrationView, "User Details fields are empty");
                return;
            }else if(userIDField.isEmpty()){
                JOptionPane.showMessageDialog(registrationView, "Username field is empty");
                return;
            }else if(passwordField.isEmpty()){
                JOptionPane.showMessageDialog(registrationView, "Password field is empty");
                return;
            }else if(confirmPassField.isEmpty()){
                JOptionPane.showMessageDialog(registrationView, "Confirm Password field is empty");
                return;
            }else if(emailField.isEmpty()){
                JOptionPane.showMessageDialog(registrationView, "Email Address field is empty");
                return;
            }else if(firstNameField.isEmpty() && surnameField.isEmpty() && ageField.isEmpty()){
                JOptionPane.showMessageDialog(registrationView, "Personal Information fields are empty");
                return;
            }else if(firstNameField.isEmpty()){
                JOptionPane.showMessageDialog(registrationView, "First Name field is empty");
                return;
            }else if(surnameField.isEmpty()){
                JOptionPane.showMessageDialog(registrationView, "Surname field is empty");
                return;
            }else if(ageField.isEmpty()){
                JOptionPane.showMessageDialog(registrationView, "Age field is empty");
                return;
            }else if(heightField.isEmpty() && weightField.isEmpty()){
                JOptionPane.showMessageDialog(registrationView, "Measurements field is empty");
                return;
            }else if(heightField.isEmpty()){
                JOptionPane.showMessageDialog(registrationView, "Height field is empty");
                return;
            }else if(weightField.isEmpty()){
                JOptionPane.showMessageDialog(registrationView, "Weight field is empty");
                return;
            }else if(!(passwordField.equals(confirmPassField))){
                JOptionPane.showMessageDialog(registrationView, "Passwords do not match!");
                return;
            }

            //Check if username already exists
            if(fileStorage.checkUsername(userIDField)) {
                JOptionPane.showMessageDialog(registrationView, "Username already exists, please choose a different one");
                return;
            }

            //Create and store the users details
            user = new User();

            //Check format of the email address
            if(!(validateEmail(emailField))){
                JOptionPane.showMessageDialog(registrationView, "Incorrect email format");
                return;
            }

            //Handle exceptions for incorrect type in text fields
            try {
                user.setAge(Integer.parseInt(ageField));
            }catch(NumberFormatException e){
                JOptionPane.showMessageDialog(registrationView,"Please enter an integer into the Age field");
                return;
            }

            try {
                user.setHeight(Integer.parseInt(heightField));
            }catch(NumberFormatException e){
                JOptionPane.showMessageDialog(registrationView,"Please enter an integer into the Height field");
                return;
            }

            try {
                user.setWeight(Integer.parseInt(weightField));
            }catch(NumberFormatException e){
                JOptionPane.showMessageDialog(registrationView,"Please enter an integer into the Weight field");
                return;
            }

            user.setUserID(userIDField);
            user.setPassword(passwordField);
            user.setEmail(emailField);
            user.setName(firstNameField + " " + surnameField);

            fileStorage.storeUser(user);


            /*/Load the profile page for the user
            if(profileView == null)
                profileView = new Profile();
                */
        }

        private boolean validateEmail(String email){
            Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
            return matcher.find();
        }
    }

}
