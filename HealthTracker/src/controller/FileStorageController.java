package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Text;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import model.Goal;
import model.User;

/**
 * Coordinates interactions between the ModelController and the File Storage
 */
public class FileStorageController {

    private Document healthTrackerData = null;

    public FileStorageController(){
        if(healthTrackerData == null)
            readXMLFile();
    }

    /**
     * Method to store a given user
     * @param user The user details to be stored
     */
    public void storeUser(User user){
        //Check if data file already exists
        if(readXMLFile()) ///< Load in the xml file data if it exists
            addUser(user);
        else
            addFirstUser(user);
    }

    /**
     * Method to add the first user to a new xml file
     * @param user The user details to be added to the file
     */
    private void addFirstUser(User user){
        healthTrackerData = new Document();

        Element rootNode = new Element("userdata");
        healthTrackerData.setRootElement(rootNode);

        Element record = new Element("record");
        record.setAttribute("id", "0");

        Element userID = new Element("userID");
        userID.setContent(new Text(user.getUserID()));

        Element password = new Element("password");
        password.setContent(new Text(user.getPassword()));

        Element email = new Element("email");
        email.setContent(new Text(user.getEmail()));

        Element name = new Element("name");
        name.setContent(new Text(user.getName()));

        Element age = new Element("age");
        age.setContent(new Text(Integer.toString(user.getAge())));

        Element height = new Element("height");
        height.setContent(new Text(Integer.toString(user.getHeight())));

        Element weight = new Element("weight");
        weight.setContent(new Text(Integer.toString(user.getWeight())));

        //Add the details to a record
        record.addContent(userID);
        record.addContent(password);
        record.addContent(email);
        record.addContent(name);
        record.addContent(age);
        record.addContent(height);
        record.addContent(weight);

        //Add the record to the root node
        rootNode.addContent(record);

        createXMLFile();
    }

    /**
     * Method to add a user to an existing xml file
     * @param user The user details to be added
     */
    private void addUser(User user){
        //Get the root element in the file(userdata)
        Element rootNode = healthTrackerData.getRootElement();

        int noOfRecords = 0;

        for(Element record : rootNode.getChildren()){
            noOfRecords++;
        }

        Element record = new Element("record");
        record.setAttribute("id", Integer.toString(noOfRecords));

        Element userID = new Element("userID");
        userID.setContent(new Text(user.getUserID()));

        Element password = new Element("password");
        password.setContent(new Text(user.getPassword()));

        Element email = new Element("email");
        email.setContent(new Text(user.getEmail()));

        Element name = new Element("name");
        name.setContent(new Text(user.getName()));

        Element age = new Element("age");
        age.setContent(new Text(Integer.toString(user.getAge())));

        Element height = new Element("height");
        height.setContent(new Text(Integer.toString(user.getHeight())));

        Element weight = new Element("weight");
        weight.setContent(new Text(Integer.toString(user.getWeight())));

        //Add the details to a record
        record.addContent(userID);
        record.addContent(password);
        record.addContent(email);
        record.addContent(name);
        record.addContent(age);
        record.addContent(height);
        record.addContent(weight);

        rootNode.addContent(record);

        //Create a new XML file
        createXMLFile();

    }

    /**
     * Method used to store a goal
     * @param goal The goal to be stored
     * @param user The user to store the goal under
     */
    public void storeGoal(Goal goal, User user){
        //Find the matching user then store the goal
        for(Element record : healthTrackerData.getRootElement().getChildren()){
            if(record.getChildText("userID").equals(user.getUserID())){
                addGoal(record, goal);
            }
        }
    }

    /**
     * Method to add a goal to file
     * @param record the record to add the goal to
     * @param goal the details of the goal to be added
     */
    private void addGoal(Element record, Goal goal){
        Element goalRootNode = new Element("goal");

        Element name = new Element("name");
        name.setContent(new Text(goal.getName()));

        Element goalID = new Element("goal-ID");
        goalID.setContent(new Text(goal.getGoalID()));

        Element type = new Element("type");
        type.setContent(new Text(goal.getType()));

        Element deadline = new Element("deadline");
        deadline.setContent(new Text(goal.getDeadline().toString()));

        Element achieved = new Element("achieved");
        record.addContent(new Text(goal.getAchieved().toString()));

        goalRootNode.addContent(name);
        goalRootNode.addContent(goalID);
        goalRootNode.addContent(type);
        goalRootNode.addContent(deadline);
        goalRootNode.addContent(achieved);

        record.addContent(goalRootNode);

        createXMLFile();
    }

    /**
     * Method to get the fields related to the given user id
     * @param userID The user id to grab the information from
     * @return The user details
     */
    public User getUser(String userID){
        User user = new User();

        //Find the matching user then pull the information out
        for(Element record : healthTrackerData.getRootElement().getChildren()){
            if(record.getChildText("userID").equals(userID)){
                user.setUserID(record.getChildText("userID"));
                user.setPassword(record.getChildText("password"));
                user.setEmail(record.getChildText("email"));
                user.setName(record.getChildText("name"));
                user.setAge(Integer.parseInt(record.getChildText("age")));
                user.setHeight(Integer.parseInt(record.getChildText("height")));
                user.setWeight(Integer.parseInt(record.getChildText("weight")));
            }
        }

        return user;

    }

    public boolean checkUsername(String userID){
        if(readXMLFile()) {
            for (Element record : healthTrackerData.getRootElement().getChildren()) {
                if (record.getChildText("userID").equals(userID))
                    return true;
            }
        }

        return false;
    }

    /**
     * Method to create an XML file using data inputted
     */
    private void createXMLFile(){
        XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());

        try{
            xmlOutput.output(healthTrackerData, new FileOutputStream(new File("./HealthTracker/src/healthTrackerData.xml")));
        }catch (IOException e) {
           // System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Method to load an XML file into a class variable
     * @return true if file exists, else return false and print error message
     */
    private boolean readXMLFile(){
        SAXBuilder builder = new SAXBuilder();

        //Catch exceptions if file does not exist
        try{
            healthTrackerData = builder.build(new File("./HealthTracker/src/healthTrackerData.xml")); ///< Load the file into a variable
            return true;
        }catch(JDOMException | IOException e){
            return false;
        }
    }

}
