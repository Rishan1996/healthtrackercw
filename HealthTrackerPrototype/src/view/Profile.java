package view;

/* **************************
   *        Legend          *
   *------------------------*
   *     p_ = panel         *
   *     l_ = label         *
   *     b_ = button        *
   *   tf_ = text field     *
   *  pf_ = password field  *
   *    ta_ = text area     *
   *    sp_ = scroll pane   *
   **************************/

import model.User;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.time.format.DateTimeFormatter;

import javax.swing.*;
import javax.swing.border.Border;

/**
 * The User Profile Interface
 */
public class Profile extends JFrame{

    //The buttons needed
    public JButton b_addGoal = new JButton("New Goal");
    public JButton b_updateWeight = new JButton("Update");
    public JButton b_logOut = new JButton("Log Out");
    public JButton b_updateName = new JButton("Update");
    public JButton b_updateEmail = new JButton("Update");
    public JButton b_updateAge = new JButton("Update");
    public JButton b_updateHeight = new JButton("Update");
    public JButton b_changePass = new JButton("Change Password");


    public Profile(User user){
        this.setTitle("Health Tracker - Profile: " + user.getUserID()); ///< Set the title of the frame
        this.setSize(900, 500); ///< Set the size of the frame
        this.setResizable(false); ///< Make the frame not resizeable
        this.setLocationRelativeTo(null); ///< Set the position of the frame at the center of the screen

        //Set the buttons tool
        b_addGoal.setToolTipText("Click here to create a new goal");
        b_updateWeight.setToolTipText("Click here to update your weight");
        b_logOut.setToolTipText("Click here to log out");
        b_updateName.setToolTipText("Click here to update your name");
        b_updateEmail.setToolTipText("Click here to update your email");
        b_updateAge.setToolTipText("Click here to update your age");
        b_updateHeight.setToolTipText("Click here to update your height");
        b_changePass.setToolTipText("Click here to change your password");

        JPanel p_profile = new JPanel();
        JPanel p_weighings = new JPanel();
        JPanel p_goals = new JPanel();
        JPanel p_profileDetails = new JPanel();
        JPanel p_currentGoals = new JPanel();
        JPanel p_completedGoals = new JPanel();
        JPanel p_expiredGoals = new JPanel();
        JPanel p_currentWeight = new JPanel();
        JPanel p_previousWeights = new JPanel();

        Border profileBorder = BorderFactory.createTitledBorder(user.getName() + "'s Profile Page");
        Border weighingsBorder = BorderFactory.createTitledBorder("Weighings"); ///< Create the border
        Border goalsBorder = BorderFactory.createTitledBorder("Goals");
        Border profileDetailsBorder = BorderFactory.createTitledBorder("My Details");
        Border basicBorder = BorderFactory.createTitledBorder("");

        //Set the borders
        p_profile.setBorder(profileBorder);
        p_weighings.setBorder(weighingsBorder);
        p_goals.setBorder(goalsBorder);
        p_profileDetails.setBorder(profileDetailsBorder);
        p_currentGoals.setBorder(basicBorder);
        p_completedGoals.setBorder(basicBorder);
        p_expiredGoals.setBorder(basicBorder);
        p_currentWeight.setBorder(basicBorder);
        p_previousWeights.setBorder(basicBorder);

        //Set the layouts
        p_profile.setLayout(new GridBagLayout());
        p_weighings.setLayout(new GridBagLayout());
        p_goals.setLayout(new GridBagLayout());
        p_profileDetails.setLayout(new GridBagLayout());
        p_currentGoals.setLayout(new GridBagLayout());
        p_completedGoals.setLayout(new GridBagLayout());
        p_expiredGoals.setLayout(new GridBagLayout());
        p_currentWeight.setLayout(new GridBagLayout());
        p_previousWeights.setLayout(new GridBagLayout());

        //Labels
        JLabel l_currentGoals = new JLabel("Current Goals: ");
        JLabel l_completedGoals = new JLabel("Completed Goals: ");
        JLabel l_overdueGoals = new JLabel("Expired Goals");
        JLabel l_currentWeight = new JLabel("Current Weight: ");
        JLabel l_previousWeights = new JLabel("Previous Weights");
        JLabel l_date = new JLabel("Date Entered");
        JLabel l_name = new JLabel("Name: " + user.getName());
        JLabel l_userID = new JLabel("User ID: " + user.getUserID());
        JLabel l_email = new JLabel("Email Address: " + user.getEmail());
        JLabel l_age = new JLabel("Age: " + user.getAge());
        JLabel l_height = new JLabel("Height: " + user.getHeight() + " cm");
        float bmi = (user.getWeight() / ((user.getHeight()/100.0f)*(user.getHeight()/100.0f)));
        JLabel l_BMI = new JLabel("BMI: " + String.format("%.2f", bmi));

        JTextField tf_currentWeight = new JTextField(user.getWeight() + " kg", 5);
        tf_currentWeight.setEditable(false);

        JTextArea ta_previousWeights = new JTextArea("", 10, 20);
        ta_previousWeights.setEditable(false);

        int counter = 0;
        
        //Set the text area content for the previous weights
        for(int i : user.getPreviousWeights()) {
            if(i > 0) {
                ta_previousWeights.append(i + " kg");
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm dd/MM/yyyy");
                ta_previousWeights.append("\t          " + user.getDates().get(counter++).format(formatter) + "\n");
            }
        }

        JScrollPane sp_previousWeights = new JScrollPane(ta_previousWeights, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        //Add current goal label to current goal panel
        Login.addComponent(p_currentGoals, l_currentGoals, 0, 0, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        //Add current goal panel to goal panel
        Login.addComponent(p_goals, p_currentGoals, 0, 0, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        //Add completed goal label to expired goals panel
        Login.addComponent(p_completedGoals, l_completedGoals, 0, 0, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        //Add the completed goals panel
        Login.addComponent(p_goals, p_completedGoals, 0, 1, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        //Add overdue goal label to expired goals panel
        Login.addComponent(p_expiredGoals, l_overdueGoals, 0, 0, 1, 1,
        GridBagConstraints.WEST, GridBagConstraints.NONE);

        //Add the expired goals panel
        Login.addComponent(p_goals, p_expiredGoals, 0, 2, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        //Add the new goal button
        Login.addComponent(p_goals, b_addGoal, 0, 3, 1,1,
                GridBagConstraints.EAST, GridBagConstraints.NONE);


        //Add the name label to the profile details panel
        Login.addComponent(p_profileDetails, l_name, 0, 0, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        Login.addComponent(p_profileDetails, b_updateName, 1, 0, 1, 1,
                GridBagConstraints.EAST, GridBagConstraints.NONE);

        Login.addComponent(p_profileDetails, l_userID, 0, 1, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        Login.addComponent(p_profileDetails, l_email, 0, 2, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        Login.addComponent(p_profileDetails, b_updateEmail, 1, 2, 1, 1,
                GridBagConstraints.EAST, GridBagConstraints.NONE);

        Login.addComponent(p_profileDetails, l_age, 0, 3, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        Login.addComponent(p_profileDetails, b_updateAge, 1, 3, 1, 1,
                GridBagConstraints.EAST, GridBagConstraints.NONE);

        Login.addComponent(p_profileDetails, l_height, 0, 4, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        Login.addComponent(p_profileDetails, b_updateHeight, 1, 4, 1, 1,
                GridBagConstraints.EAST, GridBagConstraints.NONE);

        Login.addComponent(p_profileDetails, b_changePass, 1, 5, 1, 1,
                GridBagConstraints.EAST, GridBagConstraints.NONE);


        //Add the current weight label to the current weight panel
        Login.addComponent(p_currentWeight, l_currentWeight, 0, 0, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        Login.addComponent(p_currentWeight, tf_currentWeight, 1, 0, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        Login.addComponent(p_currentWeight, l_BMI, 2, 0, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        //Add the update weight button to current weight panel
        Login.addComponent(p_currentWeight, b_updateWeight, 0, 1, 1, 1,
                GridBagConstraints.EAST, GridBagConstraints.NONE);

        //Add the current weight panel to the weighings panel
        Login.addComponent(p_weighings, p_currentWeight, 0, 0, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        //Add the previous weights label to the previous weights panel
        Login.addComponent(p_previousWeights, l_previousWeights, 0, 0, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        Login.addComponent(p_previousWeights, l_date, 1, 0, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);


        Login.addComponent(p_previousWeights, sp_previousWeights, 0, 1, 4, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        //Add the previous weights panel to the weighings panel
        Login.addComponent(p_weighings, p_previousWeights, 0, 1, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);


        //Add the panels to profile
        Login.addComponent(p_profile, p_goals, 0, 0, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.WEST);

        Login.addComponent(p_profile, p_profileDetails, 1, 0, 1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.CENTER);

        Login.addComponent(p_profile, b_logOut, 2, 0, 1, 1,
                GridBagConstraints.NORTHEAST, GridBagConstraints.NONE);

        Login.addComponent(p_profile, p_weighings, 2, 0, 1,1,
                GridBagConstraints.EAST, GridBagConstraints.EAST);

        this.add(p_profile);
        this.setVisible(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public void addListeners(ActionListener listenerHandler){
        b_logOut.addActionListener(listenerHandler);
        b_addGoal.addActionListener(listenerHandler);
        b_updateName.addActionListener(listenerHandler);
        b_updateEmail.addActionListener(listenerHandler);
        b_updateAge.addActionListener(listenerHandler);
        b_updateHeight.addActionListener(listenerHandler);
        b_changePass.addActionListener(listenerHandler);
        b_updateWeight.addActionListener(listenerHandler);
    }
}
