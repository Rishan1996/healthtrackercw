package view;

/* **************************
   *        Legend          *
   *------------------------*
   *     p_ = panel         *
   *     l_ = label         *
   *     b_ = button        *
   *   tf_ = text field     *
   *  pf_ = password field  *
   **************************/

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.Border;


/**
 * The Registration Interface
 */
public class Registration extends JFrame{

    //The text fields needed
    private JTextField tf_userID = new JTextField("", 15);
    private JPasswordField pf_password = new JPasswordField("", 15);
    private JPasswordField pf_passConfirm = new JPasswordField("", 15);
    private JTextField tf_email = new JTextField("", 15);
    private JTextField tf_firstName = new JTextField("", 15);
    private JTextField tf_surname = new JTextField("", 15);
    private JTextField tf_age = new JTextField("", 5);
    private JTextField tf_height = new JTextField("", 5);
    private JTextField tf_weight = new JTextField("", 5);

    //The buttons needed
    public JButton b_cancel = new JButton("Cancel");
    public JButton b_register = new JButton("Register");

    /**
     * Constructor creates the Registration interface
     */
    public Registration(){

        this.setTitle("Health Tracker - Register"); ///< Set the title of the frame
        this.setSize(400, 500); ///< Set the size of the frame
        this.setResizable(false); ///< Make the frame not resizeable
        this.setLocationRelativeTo(null); ///< Set the position of the frame at the center of the screen



        //Create the panels needed for this view
        JPanel p_registration = new JPanel();
        JPanel p_userDetails = new JPanel();
        JPanel p_personalInfo = new JPanel();
        JPanel p_measurements = new JPanel();
        JPanel p_buttons = new JPanel();


        //Create the borders for the panels
        Border mainBorder = BorderFactory.createTitledBorder("Registration");
        Border userDetailsBorder = BorderFactory.createTitledBorder("User Details");
        Border personalInfoBorder = BorderFactory.createTitledBorder("Personal Information");
        Border measurementsBorder = BorderFactory.createTitledBorder("Measurements");


        //Set the borders for the panels
        p_registration.setBorder(mainBorder);
        p_userDetails.setBorder(userDetailsBorder);
        p_personalInfo.setBorder(personalInfoBorder);
        p_measurements.setBorder(measurementsBorder);


        //Set the layout of the panels
        p_registration.setLayout(new GridBagLayout());
        p_userDetails.setLayout(new GridBagLayout());
        p_personalInfo.setLayout(new GridBagLayout());
        p_measurements.setLayout(new GridBagLayout());
        p_buttons.setLayout(new GridBagLayout());


        //Create and add the labels and the text fields to the User Detail's Panel
        JLabel l_userID = new JLabel("User ID: "); ///< User ID label
        Login.addComponent(p_userDetails, l_userID, 0, 0, 1, 1,
                     GridBagConstraints.WEST, GridBagConstraints.NONE);

        tf_userID.setToolTipText("Enter preferred user ID"); ///< User ID text field tooltip
        Login.addComponent(p_userDetails, tf_userID, 1, 0, 2, 1,
                     GridBagConstraints.EAST, GridBagConstraints.NONE);

        JLabel l_password = new JLabel("Password: "); ///< Password label
        Login.addComponent(p_userDetails, l_password, 0, 1, 1, 1,
                     GridBagConstraints.WEST, GridBagConstraints.NONE);

        pf_password.setToolTipText("Enter preferred password"); ///< Password text field tooltip
        Login.addComponent(p_userDetails, pf_password, 1, 1, 2, 1,
                     GridBagConstraints.EAST, GridBagConstraints.NONE);

        JLabel l_passConfirm = new JLabel("Confirm Password: "); ///< Confirm password label
        Login.addComponent(p_userDetails, l_passConfirm, 0, 2, 1, 1,
                     GridBagConstraints.WEST, GridBagConstraints.NONE);

        pf_passConfirm.setToolTipText("Confirm your password"); ///< Confirm password text field tooltip
        Login.addComponent(p_userDetails,pf_passConfirm, 1, 2, 2, 1,
                     GridBagConstraints.EAST, GridBagConstraints.NONE);

        JLabel l_email = new JLabel("Email Address: "); ///< Email label
        Login.addComponent(p_userDetails, l_email, 0, 3, 1, 1,
                     GridBagConstraints.WEST, GridBagConstraints.NONE);

        tf_email.setToolTipText("Enter your email address"); ///< Email textfield tooltip
        Login.addComponent(p_userDetails, tf_email, 1, 3, 2, 1,
                     GridBagConstraints.EAST, GridBagConstraints.NONE);

        //Create and add the labels and the text fields to the Personal Information Panel
        JLabel l_firstName = new JLabel("First Name: "); ///< First name label
        Login.addComponent(p_personalInfo, l_firstName, 0, 0, 1, 1,
                     GridBagConstraints.WEST, GridBagConstraints.NONE);

        tf_firstName.setToolTipText("Enter your first name"); ///< First name text field tooltip
        Login.addComponent(p_personalInfo, tf_firstName, 1, 0, 2, 1,
                     GridBagConstraints.EAST, GridBagConstraints.NONE);

        JLabel l_surname = new JLabel("Surname: "); ///< Surname label
        Login.addComponent(p_personalInfo, l_surname, 0, 1, 1, 1,
                     GridBagConstraints.WEST, GridBagConstraints.NONE);

        tf_surname.setToolTipText("Enter your surname"); ///< Surname text field tooltip
        Login.addComponent(p_personalInfo, tf_surname, 1, 1, 2, 1,
                     GridBagConstraints.EAST, GridBagConstraints.NONE);

        JLabel l_age = new JLabel("Age: ");  ///< Age label
        Login.addComponent(p_personalInfo, l_age, 0, 2, 1, 1,
                     GridBagConstraints.WEST, GridBagConstraints.NONE);

        tf_age.setToolTipText("Enter your age"); ///< Age text field tooltip
        Login.addComponent(p_personalInfo, tf_age, 1, 2, 2, 1,
                     GridBagConstraints.EAST, GridBagConstraints.NONE);

        //Create and add the labels and the text fields to the Measurement's Panel
        JLabel l_height = new JLabel("Height(cm): "); ///< Height label
        Login.addComponent(p_measurements, l_height, 0, 0, 1, 1,
                     GridBagConstraints.WEST, GridBagConstraints.NONE);

        tf_height.setToolTipText("Enter your height(cm)"); ///< Height text field tooltip
        Login.addComponent(p_measurements, tf_height, 1, 0, 2, 1,
                     GridBagConstraints.EAST, GridBagConstraints.NONE);

        JLabel l_weight = new JLabel("Weight(kg): "); ///< Weight label
        Login.addComponent(p_measurements, l_weight, 0, 1, 1, 1,
                     GridBagConstraints.WEST, GridBagConstraints.NONE);

        tf_weight.setToolTipText("Enter your weight in kilograms"); ///< Weight text field tooltip
        Login.addComponent(p_measurements, tf_weight, 1, 1, 2, 1,
                     GridBagConstraints.EAST, GridBagConstraints.NONE);

        //Create and add the buttons to the Button's panel
        b_cancel.setToolTipText("Press to cancel registration and to return to the login screen");
        Login.addComponent(p_buttons, b_cancel, 0, 0, 1, 1,
                     GridBagConstraints.WEST, GridBagConstraints.NONE);

        b_register.setToolTipText("Press to complete registration");
        Login.addComponent(p_buttons, b_register, 1, 0, 1, 1,
                     GridBagConstraints.EAST, GridBagConstraints.NONE);


        //Add the panels to the main panel
        Login.addComponent(p_registration, p_userDetails, 0, 0, 2, 4,
                     GridBagConstraints.NORTH, GridBagConstraints.NONE);
        Login.addComponent(p_registration, p_personalInfo, 0, 4, 2, 3,
                     GridBagConstraints.NORTH, GridBagConstraints.NONE);
        Login.addComponent(p_registration, p_measurements, 0, 7, 2, 2,
                     GridBagConstraints.NORTH, GridBagConstraints.NONE);
        Login.addComponent(p_registration, p_buttons, 0, 9, 2, 1,
                     GridBagConstraints.NORTH, GridBagConstraints.NONE);


        this.add(p_registration); ///< Add the main panel to the frame
        this.setVisible(true); ///< Show the frame
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); ///< Exit when close button is clicked
    }

    /**
     * To get the Username
     * @return the text in the userName text field
     */
    public String getUserID(){
        return tf_userID.getText();
    }

    /**
     * To get the Password
     * @return the text in the password text field
     */
    public String getPassword(){
        return new String(pf_password.getPassword());
    }

    /**
     * To get the Confirmed Password
     * @return the text in the passConfirm text field
     */
    public String getConfirmPassword(){
        return new String(pf_passConfirm.getPassword());
    }

    /**
     * To get the Email
     * @return the text in the email text field
     */
    public String getEmail(){
        return tf_email.getText();
    }

    /**
     * To get the First Name
     * @return the text in the firstName text field
     */
    public String getFirstName(){
        return tf_firstName.getText();
    }

    /**
     * To get the Surname
     * @return the text in the surname text field
     */
    public String getSurname(){
        return tf_surname.getText();
    }

    /**
     * To get the Age
     * @return the text in the age text field
     */
    public String getAge(){
        return tf_age.getText();
    }

    /**
     * To get the Height
     * @return the text in the height text field
     */
    public String getUserHeight(){
        return tf_height.getText();
    }

    /**
     * To get the Weight
     * @return the text in the weight text field
     */
    public String getWeight(){
        return tf_weight.getText();
    }

    /**
     * Listen for the Cancel button
     * @param listenerHandler
     */
    public void addListeners(ActionListener listenerHandler){
        b_cancel.addActionListener(listenerHandler);
        b_register.addActionListener(listenerHandler);
    }

}
