package controller;

import model.Goal;
import model.User;

import view.Registration;
import view.Login;
import view.Profile;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Coordinates interactions between the Views and the Models
 */
public class ModelController {

    //The views
    private Login loginView = null;
    private Registration registrationView = null;
    private Profile profileView = null;
    private JDialog d_addGoal;

    private JFrame currentView = null;

    private JButton b_cancelGoal;
    private JButton b_addGoal;

    //The file storage controller
    FileStorageController fileStorage = new FileStorageController();

    //The models
    private User user = null;
    private Goal currentGoal = new Goal();

    /**
     * The constructor loads the views and sets up the listeners
     */
    public ModelController(){

        if(this.loginView == null)
            displayLoginView();

    }

    private void displayProfile(User currentUser){
        if(registrationView != null) {
            registrationView.dispose();
            registrationView.setVisible(false);
        }

        if(loginView != null) {
            loginView.dispose();
            loginView.setVisible(false);
        }

        profileView = new Profile(currentUser);
        profileView.addListeners(new ProfileListener());

        profileView.setVisible(true);
    }

    private boolean createGoal(User currentUser, String goalName, String userName){

        return false;
    }

    public boolean checkForGoals(){
        return false;
    }

    private void addGoal(){
        d_addGoal= new JDialog(profileView, "Create Goal", true);
        JPanel p_addGoal = new JPanel();

        p_addGoal.setLayout(new GridBagLayout());

        JLabel l_goalName = new JLabel("Goal Name: ");
        JLabel l_goalType = new JLabel("Goal Type: ");
        JLabel l_targetWeight = new JLabel("Target Weight(kg): ");
        JLabel l_goalDeadline = new JLabel("Goal Deadline: ");

        JTextField tf_goalName = new JTextField("", 15);
        String choices[] = {"Losing Weight", "Gaining Weight"};
        JComboBox<String> cb_goalType = new JComboBox<>(choices);
        JTextField tf_targetWeight = new JTextField("", 5);
        JTextField tf_goalDeadline = new JTextField("dd/mm/yyyy", 15);

        b_cancelGoal = new JButton("Cancel");
        b_addGoal = new JButton("Add Goal");

        //Add the goal name label to the add goal panel
        Login.addComponent(p_addGoal, l_goalName, 0, 0, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        //Add the goal name text field to the add goal panel
        Login.addComponent(p_addGoal, tf_goalName, 1, 0, 1, 1,
                GridBagConstraints.EAST, GridBagConstraints.NONE);

        Login.addComponent(p_addGoal, l_goalType, 0, 1, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        Login.addComponent(p_addGoal, cb_goalType, 1, 1, 1, 1,
                GridBagConstraints.EAST, GridBagConstraints.NONE);

        cb_goalType.setVisible(true);

        //Add the target weight label to the add goal panel
        Login.addComponent(p_addGoal, l_targetWeight, 0, 2, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        //Add the target weight text field to the add goal panel
        Login.addComponent(p_addGoal, tf_targetWeight, 1, 2, 1, 1,
                GridBagConstraints.EAST, GridBagConstraints.NONE);

        Login.addComponent(p_addGoal, l_goalDeadline, 0, 3, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        Login.addComponent(p_addGoal, tf_goalDeadline, 1, 3, 1, 1,
                GridBagConstraints.EAST, GridBagConstraints.NONE);

        Login.addComponent(p_addGoal, b_cancelGoal, 0, 4, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.NONE);

        b_cancelGoal.addActionListener(new GoalListener());

        Login.addComponent(p_addGoal, b_addGoal, 1, 4, 1, 1,
                GridBagConstraints.EAST, GridBagConstraints.NONE);

        b_addGoal.addActionListener(new GoalListener());


        d_addGoal.add(p_addGoal);
        d_addGoal.setSize(400, 400);
        d_addGoal.setVisible(true);
        d_addGoal.setResizable(false);
        d_addGoal.setLocationRelativeTo(null);
        d_addGoal.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    }

    private void displayLoginView(){
        this.loginView = new Login();
        this.loginView.setVisible(true);
        this.loginView.addListeners(new LoginListener());
    }


    private class LoginListener implements ActionListener{

        public void actionPerformed(ActionEvent event) {
            //Handle events for the login button
            if (event.getSource() == loginView.b_login){
                String userIDField = loginView.getUserID();
                String passwordField = loginView.getPassword();

                //Check if both the username and password field is empty
                if(userIDField.isEmpty() && passwordField.isEmpty()){
                    JOptionPane.showMessageDialog(loginView, "Username and password field is empty");
                    return;
                    //Check if the username field is empty or contains just a space
                }else if(userIDField.isEmpty() || userIDField.equals(" ")){
                    JOptionPane.showMessageDialog(loginView, "Username field is empty or invalid");
                    return;
                    //Check if the password field is empty or contains a space
                }else if(passwordField.isEmpty() || passwordField.equals(" ")){
                    JOptionPane.showMessageDialog(loginView, "Password field is empty or invalid");
                    return;
                }
                
                //Check if username exists
                if(!fileStorage.checkUsername(userIDField)){
                    JOptionPane.showMessageDialog(loginView, "Username does not exist");
                    return;
                }

                user = fileStorage.getUser(userIDField);
                
                //Attempt to login               
                if (user.checkCredential(userIDField, passwordField))
                    displayProfile(user); ///< Load the profile page
                else
                    JOptionPane.showMessageDialog(loginView, "Incorrect credentials");

            }else if(event.getSource() == loginView.b_signUp){ //Handle events for the sign up button
                loginView.dispose();
                loginView.setVisible(false);

                registrationView = new Registration();
                registrationView.setVisible(true);
                registrationView.addListeners(new RegistrationListener());
            }

        }
    }

    class RegistrationListener implements ActionListener{

        public void actionPerformed(ActionEvent event) {

            //Handle events for the login button
            if (event.getSource() == registrationView.b_cancel){
                registrationView.dispose();
                registrationView.setVisible(false);

                displayLoginView();
            }else if(event.getSource() == registrationView.b_register){
                final Pattern VALID_EMAIL_ADDRESS_REGEX =
                        Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

                String userIDField = registrationView.getUserID();
                String passwordField = registrationView.getPassword();
                String confirmPassField = registrationView.getConfirmPassword();
                String emailField = registrationView.getEmail();

                String firstNameField = registrationView.getFirstName();
                String surnameField = registrationView.getSurname();
                String ageField = registrationView.getAge();

                String heightField = registrationView.getUserHeight();
                String weightField = registrationView.getWeight();

                //Check if any fields are empty
                if(userIDField.isEmpty() && passwordField.isEmpty() && confirmPassField.isEmpty() && emailField.isEmpty()){
                    JOptionPane.showMessageDialog(registrationView, "User Details fields are empty");
                    return;
                }else if(userIDField.isEmpty()){
                    JOptionPane.showMessageDialog(registrationView, "Username field is empty");
                    return;
                }else if(passwordField.isEmpty()){
                    JOptionPane.showMessageDialog(registrationView, "Password field is empty");
                    return;
                }else if(confirmPassField.isEmpty()){
                    JOptionPane.showMessageDialog(registrationView, "Confirm Password field is empty");
                    return;
                }else if(emailField.isEmpty()){
                    JOptionPane.showMessageDialog(registrationView, "Email Address field is empty");
                    return;
                }else if(firstNameField.isEmpty() && surnameField.isEmpty() && ageField.isEmpty()){
                    JOptionPane.showMessageDialog(registrationView, "Personal Information fields are empty");
                    return;
                }else if(firstNameField.isEmpty()){
                    JOptionPane.showMessageDialog(registrationView, "First Name field is empty");
                    return;
                }else if(surnameField.isEmpty()){
                    JOptionPane.showMessageDialog(registrationView, "Surname field is empty");
                    return;
                }else if(ageField.isEmpty()){
                    JOptionPane.showMessageDialog(registrationView, "Age field is empty");
                    return;
                }else if(heightField.isEmpty() && weightField.isEmpty()){
                    JOptionPane.showMessageDialog(registrationView, "Measurements field is empty");
                    return;
                }else if(heightField.isEmpty()){
                    JOptionPane.showMessageDialog(registrationView, "Height field is empty");
                    return;
                }else if(weightField.isEmpty()){
                    JOptionPane.showMessageDialog(registrationView, "Weight field is empty");
                    return;
                }else if(!(passwordField.equals(confirmPassField))){
                    JOptionPane.showMessageDialog(registrationView, "Passwords do not match!");
                    return;
                }

                //Check if username already exists
                if(fileStorage.checkUsername(userIDField)) {
                    JOptionPane.showMessageDialog(registrationView, "Username already exists, please choose a different one");
                    return;
                }

                //Create and store the users details
                user = new User();

                Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailField);

                //Check format of the email address
                if(!(matcher.find())){
                    JOptionPane.showMessageDialog(registrationView, "Incorrect email format");
                    return;
                }else
                    user.setEmail(emailField);


                //Handle exceptions for incorrect type in text fields
                try {
                    user.setAge(Integer.parseInt(ageField));
                }catch(NumberFormatException e){
                    JOptionPane.showMessageDialog(registrationView,"Please enter an integer into the Age field");
                    return;
                }

                try {
                    user.setHeight(Integer.parseInt(heightField));
                }catch(NumberFormatException e){
                    JOptionPane.showMessageDialog(registrationView,"Please enter an integer into the Height field");
                    return;
                }

                try {
                    user.setWeight(Integer.parseInt(weightField));
                }catch(NumberFormatException e){
                    JOptionPane.showMessageDialog(registrationView,"Please enter an integer into the Weight field");
                    return;
                }

                user.setUserID(userIDField);
                user.setPassword(passwordField);
                user.setName(firstNameField + " " + surnameField);

                fileStorage.storeUser(user);

                displayProfile(user);
            }
        }
    }

    class ProfileListener implements ActionListener{

        public void actionPerformed(ActionEvent event){

            //Handle events for the login button
            if (event.getSource() == profileView.b_logOut){
                profileView.dispose();
                profileView.setVisible(false);

                displayLoginView();
            }else if(event.getSource() == profileView.b_updateName){
                String newName = JOptionPane.showInputDialog("Please input a new full name: ");

                if(!newName.equals("")){
                    fileStorage.editElement("name", newName, user.getUserID());
                    user = fileStorage.getUser(user.getUserID());
                    profileView.dispose();
                    displayProfile(user);
                }
            }else if(event.getSource() == profileView.b_updateEmail){
                String newName = JOptionPane.showInputDialog("Please input a new email: ");

                final Pattern VALID_EMAIL_ADDRESS_REGEX =
                        Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
                Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(newName);
                
                if(matcher.find()){
                    fileStorage.editElement("email", newName, user.getUserID());
                    user = fileStorage.getUser(user.getUserID());
                    profileView.dispose();
                    displayProfile(user);
                }else{
                    JOptionPane.showMessageDialog(profileView, "Incorrect email format");
                    return;
                }
            }else if(event.getSource() == profileView.b_updateAge){
                String newName = JOptionPane.showInputDialog("Please input a new age: ");

                if(!newName.equals("")){
                    try{
                        Integer.parseInt(newName);
                        fileStorage.editElement("age", newName, user.getUserID());    
                        user = fileStorage.getUser(user.getUserID());
                        profileView.dispose();
                        displayProfile(user);
                    }catch(NumberFormatException e){
                        JOptionPane.showMessageDialog(profileView, "Please enter a number");
                    }
                    
                }
            }else if(event.getSource() == profileView.b_updateHeight){
                String newName = JOptionPane.showInputDialog("Please input a new height: ");

                if(!newName.equals("")){                  
                    try{
                        Integer.parseInt(newName);
                        fileStorage.editElement("height", newName, user.getUserID());
                        user = fileStorage.getUser(user.getUserID());
                        profileView.dispose();
                        displayProfile(user);
                    }catch(NumberFormatException e){
                        JOptionPane.showMessageDialog(profileView, "Please enter a number");
                    }                                     
                }
            }else if(event.getSource() == profileView.b_changePass){
                String newName = JOptionPane.showInputDialog("Please input a new password: ");
                
                if(!newName.equals("")){
                    fileStorage.editElement("password", newName, user.getUserID());              
                    user = fileStorage.getUser(user.getUserID());
                    profileView.dispose();
                    displayProfile(user);
                }
            }else if(event.getSource() == profileView.b_updateWeight){
                String newName = JOptionPane.showInputDialog("Please input a new weight: ");

                if(!newName.equals("")){
                    try{
                        Integer.parseInt(newName);
                        fileStorage.updateWeight(user.getUserID(), newName);             
                        user = fileStorage.getUser(user.getUserID());
                        profileView.dispose();
                        displayProfile(user);
                    }catch(NumberFormatException e){
                        JOptionPane.showMessageDialog(profileView, "Please enter a number");
                    } 
                }
            }else if(event.getSource() == profileView.b_addGoal){
                addGoal();
            }

        }
    }

    class GoalListener implements ActionListener{

        public void actionPerformed(ActionEvent event) {
            if(event.getSource() == b_cancelGoal){
                d_addGoal.dispose();
            }else if(event.getSource() == b_addGoal){
                Goal tempGoal = new Goal();
                tempGoal.setAchieved(false);
              //  tempGoal.setGoalID("0");
            //    tempGoal.setName(name);
             //   tempGoal.setTargetWeight(0);

            }
        }
    }



}
