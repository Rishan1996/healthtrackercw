package model;

import java.time.*;

/**
 * Model of the Goal
 */
public class Goal {

    private String name = "";
    private String goalID = "";
    private String type = "";
    private LocalDateTime deadline = LocalDateTime.now();
    private int targetWeight = 0;
    private Boolean achieved = false;

    /**
     * Default Constructor
     */
    public Goal(){

    }

    /**
     * Constructor to set all the fields of Goal
     * @param name The title of the goal
     * @param goalID The userID this goal is associated with
     * @param type The type of goal it is
     * @param deadline The deadline for the goal
     * @param targetWeight The target weight
     * @param achieved If the goal has been achieved
     */
    public Goal(String name, String goalID, String type, LocalDateTime deadline, int targetWeight, Boolean achieved){
        this.name = name;
        this.goalID = goalID;
        this.type = type;
        this.deadline = deadline;
        this.achieved = achieved;
        this.targetWeight = targetWeight;
    }



    //Accessor Methods
    public String getName(){return name;}
    public String getGoalID(){return goalID;}
    public String getType(){return type;}
    public LocalDateTime getDeadline(){return deadline;}
    public Boolean getAchieved(){return achieved;}
    public int getTargetWeight(){return targetWeight;}

    //Mutator Methods
    public void setName(String name){this.name = name;}
    public void setGoalID(String goalID){this.goalID = goalID; }
    public void setType(String type){this.type = type;}
    public void setDeadline(LocalDateTime deadline){this.deadline = deadline;}
    public void setAchieved(Boolean achieved){this.achieved = achieved;}
    public void setTargetWeight(int targetWeight){this.targetWeight = targetWeight;}


}
